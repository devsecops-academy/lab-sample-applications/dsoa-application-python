from typing import List, Dict
from flask import Flask, render_template
import mysql.connector

app = Flask(__name__)


def customers() -> List[Dict]:
    config = {
        'user': '82618201',
        'password': 'animism-sort-galvanic-angina-camp',
        'host': 'db',
        'port': '3306',
        'database': 'demo'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    cursor.execute('SELECT * FROM customer;')
    results = cursor.fetchall()
    cursor.close()
    connection.close()

    return results


@app.route('/app/')
def index() -> str:
    return render_template("customers.html", data=customers())


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8011)
